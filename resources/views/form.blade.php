<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
        <body>
            
            <form action="{{ route('welcome') }}">
                
                <h1>Buat Account Baru!</h1>
                
                <h3>Sign Up Form</h3>
                
                <p>First name:</p>
                <input type="text" name="firstname">
                <p>Last name:</p>
                <input type="text" name="lastname">
                <p>Gender:</p>
                <input type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label>
                <br>
                <input type="radio" id="female" name="gender" value="female">
                <label for="female">Female</label>
                <br>
                <input type="radio" id="other" name="gender" value="other">
                <label for="other">Other</label>
                <br>
                
                <p>Nationality:</p>
                <select name="nationality" id="nationality">
                    <option value="indonesian">Indonesian</option>
                    <option value="singaporean">Singaporean</option>
                    <option value="malaysian">Malaysian</option>
                    <option value="australian">Australian</option>
                </select>
                
                <p>Language Spoken:</p>
                <input type="checkbox" id="idn" name="language" value="idn">
                <label for="idn">Bahasa Indonesia</label>
                <br>
                <input type="checkbox" id="eng" name="language" value="eng">
                <label for="eng">English</label>
                <br>
                <input type="checkbox" id="other" name="language" value="other">
                <label for="other">Other</label>
                
                <p>Bio:</p>
                <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
                <br>
                <input type="submit"></input>

            </form>
                
            
        </body>
</html>